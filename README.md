Extracted from: https://github.com/starkandwayne/bucc-bbr-boshrelease/  

Add and customize this opsfile to collocate the bbr-atcdb and database-backup-restorer jobs on the atc vm

```yml
- type: replace
  path: /releases/-
    name: backup-and-restore-sdk
    url: https://bosh.io/d/github.com/cloudfoundry-incubator/backup-and-restore-sdk-release?v=((bbr_sdk_version))
    sha1: ((bbr_sdk_sha1))
    version: ((bbr_sdk_version))

- type: replace
  path: /releases/-
  value:
    name: bbr-atcdb
    url: https://storage.googleapis.com/bosh-release-jwi/bbr-atcdb-v1.tgz
    sha1: '48081a086f3441ab4515586ed725514117d75e34'
    version: '1'

- type: replace
  path: /instance_groups/name=web/jobs/-
  value:
    name: bbr-atcdb
    release: bbr-atcdb
    consumes:
      postgres: {from: db} # bosh link name from the ATC job
    properties:
      postgresql:
        database: atc
        role:
          name: concourse
          password: ((postgres_concourse_password))

- type: replace
  path: /instance_groups/name=web/jobs/-
  value:
    name: database-backup-restorer
    release: backup-and-restore-sdk
```